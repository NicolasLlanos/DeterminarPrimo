package cl.ubb.determinarprimo;

import static org.junit.Assert.*;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;

public class DeterminarPrimoTest {

	@Test
	public void IngresarCeroRetornaPrimoFalso() {
	   /*arrange*/
		NumeroPrimo primo = new NumeroPrimo();
	   boolean resultado;
	   
	   /*act*/
	   resultado = primo.DeterminarPrimo(0);
	   
	   
	   /*assert*/
	   assertThat(resultado,is(false));
	}
	
	

	@Test
	public void IngresarUnoRetornaPrimoFalso() {
	   /*arrange*/
		NumeroPrimo primo = new NumeroPrimo();
	   boolean resultado;
	   
	   /*act*/
	   resultado = primo.DeterminarPrimo(1);
	   
	   
	   /*assert*/
	   assertThat(resultado,is(false));
	}
	
	

	@Test
	public void IngresarDosRetornaPrimoVerdadero() {
	   /*arrange*/
		NumeroPrimo primo = new NumeroPrimo();
	   boolean resultado;
	   
	   /*act*/
	   resultado = primo.DeterminarPrimo(2);
	   
	   
	   /*assert*/
	   assertThat(resultado,is(true));
	}
	
	@Test
	public void IngresarTresRetornaPrimoVerdadero() {
	   /*arrange*/
		NumeroPrimo primo = new NumeroPrimo();
	   boolean resultado;
	   
	   /*act*/
	   resultado = primo.DeterminarPrimo(3);
	   
	   
	   /*assert*/
	   assertThat(resultado,is(true));
	}

	
	@Test
	public void IngresarCuatroRetornaPrimoFalso() {
	   /*arrange*/
		NumeroPrimo primo = new NumeroPrimo();
	   boolean resultado;
	   
	   /*act*/
	   resultado = primo.DeterminarPrimo(4);
	   
	   
	   /*assert*/
	   assertThat(resultado,is(false));
	}

}
